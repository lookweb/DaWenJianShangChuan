﻿$(function () {
    $('.uploadfile input').on('change', function () {
        if (this.files && this.files.length) {
            var bytesPerPiece = 1024 * 1024 * 2; // 每个文件切片大小定为2Mb .
            var totalPieces;//切片总数，这个根据 文件大小来计算
            var start = 0;
            var end;
            var index = 0;
            var filesize = this.files[0].size;// 文件大小
            var filename = this.files[0].name;// 文件名称
            //计算文件切片总数
            totalPieces = Math.ceil(filesize / bytesPerPiece);
            while (start < filesize) {
                end = start + bytesPerPiece;
                if (end > filesize) {
                    end = filesize;
                } // 匹配最后一个分片的情况
                var chunk = this.files[0].slice(start, end);//切割文件
                var sliceIndex = this.files[0].name;
                var formData = new FormData();
                formData.append("totalPieces", totalPieces);
                formData.append("index", index);
                formData.append("file", chunk, sliceIndex);
                $.ajax({
                    url: '/ashx/BlodUpload.ashx',
                    type: 'POST',
                    cache: false,
                    data: formData,
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    success: function (data) {
                            console.info(data.msg);
                        if (data.val) {
                            $(".filepath").val(data.val);
                        }
                    }
                }).done(function (res) {

                }).fail(function (res) {

                });
                start = end;
                index++;
            }
        }
    });
})