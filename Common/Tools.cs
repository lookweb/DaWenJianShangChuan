﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace Common
{
    public static class Tools
    {

        /// <summary>
        /// 返回SessionID
        /// </summary>
        /// <returns></returns>
        public static string ReturnSessionID()
        {
            return HttpContext.Current.Session.SessionID;
        }
        /// <summary>
        /// 获取当前值
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static object GetCacheValue(string key)
        {
            return HttpRuntime.Cache.Get(key);
        }

        /// <summary>
        /// 插入缓存值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void InsertCache(string key, object value)
        {
            HttpRuntime.Cache.Insert(key, value);
        }
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void UpdateCache(string key, object value)
        {
            HttpRuntime.Cache.Insert(key, value);
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="key"></param>
        public static void DelCache(string key)
        {
            HttpRuntime.Cache.Remove(key);
        }

        /// <summary>
        /// 判断文件夹是否存在
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static bool PathExists(string path)
        {
            //获取完整的路径
            string allpath=HttpContext.Current.Server.MapPath(path);
            return Directory.Exists(allpath);
        }

        /// <summary>
        /// 创建文件路径
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static bool CreatePath(string path)
        {
            bool status = true;
            try
            {
                //获取完整的路径
                string allpath = HttpContext.Current.Server.MapPath(path);
                Directory.CreateDirectory(allpath);
            }
            catch (Exception)
            {
                status = false;
            }

            return status;
        }

    }
}
